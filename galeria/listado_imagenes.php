<?php 

/*LISTADO DE SECCIONES*/

session_start();

require '../admin/config.php';
require '../funciones.php';

comprobarSesion();

$origen = "";
$destino = "";

$usuario = $_SESSION['usuario'];

$mensajeError = '';

$conexion = conexion($bd_config);

if (!$conexion) {
    $mensajeError .= 'Se ha producido un error de conexión a la Base de Datos';
    echo $mensajeError;
    //header('Location: error.php');

} 

/*Determina si se muestran los contenidos de un idioma u otro mediante el envío del
parámetro por GET*/
if (isset($_GET['lan'])) {
    $idioma = $_GET['lan'];
    
} else {
    $idioma = "es";
}

$totalImagenes = (int)total_imagenes($conexion, $idioma);
// $orden = $totalArticulos + 1;

$imagenes = obtener_imagenes($conexion, $idioma);

/*Se muestra las entradas que se muestran en el apartado de contacto
y que corresponden a los datos del propietario de la web, 
establecido en el archivo config.php*/
$usuario = $blog_config['usuarioContactos'];

$usuarios = obtener_usuario($conexion, $usuario);

if (!$usuarios) {
    $mensajeError .= 'No se ha recibido información de la base de datos';
    // echo $mensajeError;
    header('Location: error.php');
}

if (isset($_GET['ord'])) {
    $posicionOrigen = (int)$_GET['ord'];
    $posicionDestino = (int)$_GET['nord'];
    subirBajarImagen($conexion, $posicionOrigen, $posicionDestino);
    header('Location: listado_imagenes.php');
}


// FUNCIONES PROPIAS

/*Para obtener las imagenes de la galeria*/
function obtener_imagenes($conexion, $idioma){ //en el tutorial es obtener_post
    $totalImagenes = $conexion->prepare('SELECT * FROM galeria_img WHERE idioma = :idioma ORDER BY orden');
    $totalImagenes->execute(array(':idioma' => $idioma));
    return $totalImagenes->fetchAll();
}

/*Obtener una imagen por su orden*/
function obtener_imagen_por_orden($conexion, $orden){
    $resultado = $conexion->query("SELECT * FROM galeria_img WHERE orden = $orden LIMIT 1");
    $resultado = $resultado->fetch();
    return ($resultado) ? $resultado : false;
}

function subirBajarImagen($conexion, $posicionOrigen, $posicionDestino){

    $origen = obtener_imagen_por_orden($conexion,$posicionOrigen);
    $destino =  obtener_imagen_por_orden($conexion,$posicionDestino);

    $ordenOrigen = (int)$origen['orden'];
    $idOrigen = (int)$origen['id'];
    $ordenDestino = (int)$destino['orden'];
    $idDestino = (int)$destino['id'];

    $insertarOrdenDestino = $conexion->prepare('UPDATE galeria_img SET orden = :ordenDestino WHERE id = :idOrigen');
    $insertarOrdenDestino->execute(array(
            ':ordenDestino' => $ordenDestino, 
            ':idOrigen' => $idOrigen
    ));

    $insertarOrdenOrigen = $conexion->prepare('UPDATE galeria_img SET orden = :ordenOrigen WHERE id = :idDestino');
    $insertarOrdenOrigen->execute(array(
            ':ordenOrigen' => $ordenOrigen, 
            ':idDestino' => $idDestino
    ));

    // header('Location: listado.php');
}

/*Para conocer cuántos artículos hay en total. Es útil para establecer el número de orden
de los artículos en la BD. Posteriormente se debería permitir que el orden lo establezca
el usuario*/
function total_imagenes($conexion, $idioma){
    $total_imagenes = $conexion->prepare('SELECT * FROM galeria_img WHERE idioma = :idioma');
    $total_imagenes->execute(array(':idioma' => $idioma));
    $total_imagenes = $total_imagenes->rowCount();
    return $total_imagenes;
}

require '../galeria/view/listado_imagenes.view.php';
 ?>